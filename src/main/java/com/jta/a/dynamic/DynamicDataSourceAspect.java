package com.jta.a.dynamic;

import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * 动态数据源切面
 */
@Aspect
@Order(-1) //保证该AOP在事务之前执行
@Component
public class DynamicDataSourceAspect {
    private static final Logger logger = LoggerFactory.getLogger(DynamicDataSourceAspect.class);

    @Before("@annotation(ds)")
    public void changeDataSource(Joinpoint point,DS ds) throws Throwable{
        String dsId = ds.value();
        if(!DynamicDataSourceContextHolder.existDataSource(dsId)){
            logger.error("数据源[{}],在使用默认数据源->{}",ds.value(),point.getStaticPart());
        }else{
            logger.debug("使用的数据是 : {} > {}", ds.value(), point.getStaticPart());
            DynamicDataSourceContextHolder.setDataSourcesType(ds.value());
        }
    }
    @After("@annotation(ds)")
    public void restoreDataSource(Joinpoint point,DS ds){
        logger.debug("恢复数据源：{}》{}",ds.value(),point.getStaticPart());
        DynamicDataSourceContextHolder.clearDataSourceType();
    }
}
