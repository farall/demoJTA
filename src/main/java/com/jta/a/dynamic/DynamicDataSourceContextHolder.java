package com.jta.a.dynamic;

import java.util.ArrayList;
import java.util.List;

/**
 * * 核心基于ThreadLocal的切换数据源工具类
 */
public class DynamicDataSourceContextHolder {
    /**
     * 创建线程池
     */
    private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

    public static List<String> dataSourcesIds= new ArrayList<>();

    /**
     * 设置当前线程数据源
     * @param dataSourcesType
     */
    public static void setDataSourcesType(String dataSourcesType){
        contextHolder.set(dataSourcesType);
    }

    /**
     * 获得当线程数据源
     * @return 数据源名称
     */
    public static String getDataSourceType(){
        return contextHolder.get();
    }
    /**
     * 清空当前线程数据源
     */
    public static void clearDataSourceType(){
        contextHolder.remove();
    }

    /**
     * 判断指定DataSource当前线程是否存在
     * @param dataSourceId
     * @return
     */
    public static boolean existDataSource(String dataSourceId){
        return dataSourcesIds.contains(dataSourceId);
    }
}
