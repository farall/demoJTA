package com.jta.controller;

import com.jta.pojo.Student;
import com.jta.service.StudentService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class StudentController {
    @Resource
    private StudentService studentService;

    @GetMapping("/add")
    public int add() {
        Student student = new Student();
        student.setName("测试");
        return studentService.insert(student);
    }
}
