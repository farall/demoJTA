package com.jta.pojo;

import java.io.Serializable;
import lombok.Data;

@Data
public class Student implements Serializable {
    private Integer id;

    private String name;

    private static final long serialVersionUID = 1L;
}