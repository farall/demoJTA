package com.jta.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.jta.pojo.Student;

@DS("schooldb") //切换数据源
public interface StudentService{

    int deleteByPrimaryKey(Integer id);

    int insert(Student record);

    int insertSelective(Student record);

    Student selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Student record);

    int updateByPrimaryKey(Student record);

}
