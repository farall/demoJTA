package com.jta.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.jta.pojo.Userinfo;

@DS("master")
public interface UserinfoService{

    int deleteByPrimaryKey(Integer id);

    int insert(Userinfo record);

    int insertSelective(Userinfo record);

    Userinfo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Userinfo record);

    int updateByPrimaryKey(Userinfo record);

}
